<!doctype html>
<html>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/data" method="POST">
        @csrf
        <section class="name">
            <label for="firstname">First Name:</label>
            <br>
            <input id="firstname" name="firstname" type="text">
            <br><br>
            <label for="lastname">Last Name:</label>
            <br>
            <input id="lastname" name="lastname" type="text">
        </section>
        <br>
        <section class="gender">
            <span>Gender :</span>
            <br>
            <input id="male" name="gender" type="radio">
            <label for="male">Male</label>
            <br>
            <input id="female" name="gender" type="radio">
            <label for="female">Female</label>
            <br>
            <input id="other" name="gender" type="radio">
            <label for="other">Other</label>    
        </section>
        <br>
        <section class="nationality">
            <label for="nationality">Nationality:</label>
            <br>
            <select id="nationality" name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select>   
        </section>
        <br>
        <section class="language">
            <span>Language Spoken:</span>
            <br>
            <input type="checkbox" name="language" id="indonesia" value="indonesia">
            <label for="indonesia">Bahasa Indonesia</label>
            <br>
            <input type="checkbox" name="language" id="english" value="english">
            <label for="english">English</label>
            <br>
            <input type="checkbox" name="language" id="otherlang" value="otherlang">
            <label for="otherlang">Other</label>    
        </section>
        <br>
        <section class="bio">
            <label for="bio">Bio :</label>
            <br>
            <textarea id="bio" name="bio" rows="10" cols="30"></textarea>   
        </section>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</html>